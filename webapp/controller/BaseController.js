/*global history */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("ZRE200_UI_FHU.controller.BaseController", {
		/**
		 * Convenience method for accessing the router in every controller of the application.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function () {
			return this.getOwnerComponent().getRouter();
		},

		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model in every controller of the application.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Convenience method for getting the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Event handler for navigating back.
		 * It there is a history entry or an previous app-to-app navigation we go one step back in the browser history
		 * If not, it will replace the current entry of the browser history with the master route.
		 * @public
		 */
		onNavBack: function () {
			var sPreviousHash = History.getInstance().getPreviousHash(),
				oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");

			if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
				history.go(-1);
			} else {
				this.getRouter().navTo("View1", {}, true);
			}
		},

		_cleanHuModel: function (bNegoCedi) {
			var oModel = this.getView().getModel("HuModel");
			var sNegoCedi = bNegoCedi ? oModel.getData().NegoCedi : "";
			var oData = {
				"Exidv": "",
				"NegoCedi": sNegoCedi,
				"VbelnO": "",
				"VbelnI": "",
				"Werks": "",
				"Sca": "",
				"Stato": false
			};
			oModel.setData(oData);
			oModel.refresh(true);
		},

		_cleanHuMatModel: function () {
			var oModel = this.getView().getModel("HuMatModel");
			var oData = {
				aList: []
			};
			oModel.setData(oData);
			oModel.refresh(true);
		},

		_cleanMatModel: function () {
			var oModel = this.getView().getModel("MatModel");
			var oData = {
				"Exidv": "",
				"Ean11": "",
				"Matnr": "",
				"Bismt": "",
				"Maktx": "",
				"Stock": "",
				"QSca": "",
				"QRead": 0
			};
			oModel.setData(oData);
			oModel.refresh(true);
		}

	});

});