/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View8", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this._HuRimModel = new JSONModel({
				HuRim: ""
			});
			this.getView().setModel(this._HuRimModel, "huRim");
			this.getRouter().getRoute("View8").attachPatternMatched(this._onObjectMatched, this);

		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},

		_onObjectMatched: function () {
			var oHuModel = this.getView().getModel("HuModel");
			var oHuData = oHuModel.getData();
			var sSca = oHuData.Sca;
			var aSca = sSca.split("/");
			var oModel = this.getView().getModel("huRim");
			var oData = oModel.getData();
			if (aSca.length === 2) {
				oData.HuRim = +aSca[1] - (+aSca[0]);
			}
			oModel.setData(oData);
			oModel.refresh(true);

		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		_onPressInbound: function () {
			var i18n = this.getResourceBundle();
			var that = this;
			var aActions = [i18n.getText("ButtonSi"), i18n.getText("ButtonNo")];
			MessageBox.confirm(i18n.getText("TextMsgBoxConfirmV8"), {
				actions: aActions,
				emphasizedAction: aActions[0],
				onClose: function (sAction) {
					// MessageToast.show("Action selected: " + sAction);
					if (sAction === i18n.getText("ButtonSi")) {
						that._confirm();
					}
				}
			});
		},

		_confirm: function () {

			var oModel = this.getView().getModel();
			var oDataModel = this.getView().getModel("HuModel").getData();
			var oUrlParams = {
				Exidv : oDataModel.Exidv,
				NegoCedi : oDataModel.NegoCedi
			};
			oModel.callFunction("/CompleteDelivery", {
				method: "POST",
				urlParameters: oUrlParams,
				success: jQuery.proxy(this.successGetATPData, this),
				error: jQuery.proxy(this.errorGetATPData, this)

			});

		},

		successGetATPData: function (oData) {
			this.getRouter().navTo("View1");
		},

		_onPressConta: function () {
			this._cleanHuModel(true);
			this.getRouter().navTo("View2");
		}

	});

});