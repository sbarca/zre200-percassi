/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View4", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this.getRouter().getRoute("View4").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},
		
		_onObjectMatched: function () {
			var oInput = this.getView().byId("InputView4I");
			oInput.addEventDelegate({
				onAfterRendering: function () {
					oInput.focus();
				}
			});	
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		_onPressListaCollo: function () {
			this._cleanMatModel();
			this.getRouter().navTo("View5");
		},

		_onPressFineContaLettore: function () {
			this._cleanMatModel();
			this.getRouter().navTo("View5");
		},

		_onEnter: function (oEvent) {
			var sEan = oEvent.getSource().getValue();
			var oModel = this.getView().getModel();
			var oHuMatModel = this.getView().getModel("HuMatModel");
			var oMatModel = this.getView().getModel("MatModel");
			var oHuMatData = oHuMatModel.getData();
			var oMatData = oMatModel.getData();
			var i18n = this.getResourceBundle();
			var fnFindMat = function (oM) {
				return oM.Ean11 === sEan;
			};
			var nInd = oHuMatData.aList.findIndex(fnFindMat);

			if (nInd !== -1) {
				var oMat = jQuery.extend(true, {}, oHuMatData.aList[nInd]);
				oMat.QRead = (+oMat.QRead + 1).toString();
				
				var sPath = oModel.createKey("/HuMatSet", {
					Exidv: oMat.Exidv,
					NegoCedi: oMat.NegoCedi,
					Ean11: oMat.Ean11
				});

				oModel.update(sPath, oMat, {
					success: function (oData) {
						oMatData.Exidv = oMat.Exidv;
						oMatData.Matnr = oMat.Matnr;
						oMatData.Bismt = oMat.Bismt;
						oMatData.Maktx = oMat.Maktx;
						oMatData.Stock = oMat.Stock;
						oMatData.QSca = oMat.QSca;
						oMatData.QRead = oMat.QRead;
						oMatData.Ean11 = "";
						oHuMatData.aList[nInd].QRead = oMat.QRead;
						oHuMatModel.setData(oHuMatData);
						oHuMatModel.refresh(true);
					},

					error: function (oErr) {
						oMatData.Ean11 = "";
					}
				});

			} else {
				MessageBox["error"](
					i18n.getText("MatNotFound"), {
						actions: [MessageBox.Action.CLOSE],
						onClose: function () {}.bind(this)
					});
			}

		}

	});

});