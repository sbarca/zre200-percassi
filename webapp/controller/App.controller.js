sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function(BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.App", {

		onInit: function() {
			/*var oViewModel,
				fnSetAppNotBusy,
				iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();*/

		/*	oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});
			this.setModel(oViewModel, "appView");*/
			
			// this._localModel = new JSONModel({
			// 	view: ""
			// });

			/*fnSetAppNotBusy = function() {
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			};*/

			// since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
			// this.getOwnerComponent().getModel().metadataLoaded().
			// then(fnSetAppNotBusy);
			// this.getOwnerComponent().getModel().attachMetadataFailed(fnSetAppNotBusy);

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
			
			// this._loadStartupData();
			// var sRoute = this._localModel.getData().view;
			// this.getRouter().navTo(sRoute);
			
			this.getRouter().navTo("View1");
		} 
		
		/*_loadStartupData: function () {

			var sComponentId = sap.ui.core.Component.getOwnerIdFor(this.getView());
			var oComponent = sap.ui.component(sComponentId);
			var oComponentData = oComponent.getComponentData();

			if (oComponentData.startupParameters) {
				if (oComponentData.startupParameters.View) {
					this._localModel.setProperty("/view", oComponentData.startupParameters.View[0]);
				} else {
					this._localModel.setProperty("/view", "");
				}
			}

		}*/

	});

});