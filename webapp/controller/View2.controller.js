/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View2", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this.getRouter().getRoute("View2").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		_onObjectMatched: function () {
			var oModel = this.getModel("HuModel");
			var oData = oModel.getData();
			if (!oData.NegoCedi) {
				this.getRouter().navTo("View1");
			}
		},

		_onPress: function () {
			var i18n = this.getResourceBundle();
			var that = this; 
			var aActions = [i18n.getText("ButtonSi"), i18n.getText("ButtonNo")];
			MessageBox.confirm(i18n.getText("TextMsgBoxConfirm"), {
				actions: aActions,
				emphasizedAction: aActions[0],  
				onClose: function (sAction) {
					// MessageToast.show("Action selected: " + sAction);
					if (sAction === i18n.getText("ButtonSi")){
						that._confirm();
						// that.getRouter().navTo("View3");
					}
				}
			});
		},

		_confirm: function () {
			var oModel = this.getModel();
			var oHuModel = this.getModel("HuModel");
			var oHu = oHuModel.getData();
			var that = this;
			sap.ui.core.BusyIndicator.show(0);
			if (oHu.Exidv) {
				var sPath = oModel.createKey("/HuSet", {
					Exidv: oHu.Exidv,
					NegoCedi: oHu.NegoCedi
				});
				oModel.read(sPath, {
					urlParameters: {
						"$expand": "HuMatSet"
					},
					success: function (oRes) {
						sap.ui.core.BusyIndicator.hide();
						oHu.VbelnO = oRes.VbelnO;
						oHu.VbelnI = oRes.VbelnI;
						oHu.Werks = oRes.Werks;
						oHu.Sca = oRes.Sca;
						oHu.Stato = oRes.Stato;
						oHuModel.setData(oHu);
						oHuModel.refresh(true);
						if (oRes.HuMatSet && oRes.HuMatSet.results) {
							that._setHuMatData(oRes.HuMatSet.results);
						}
						that.getRouter().navTo("View3");

					},
					error: function (oErr) {
						sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},

		_setHuMatData: function (aHuMat) {
			var oModel = this.getView().getModel("HuMatModel");
			var oData = oModel.getData();
			oData.aList = [];
			for (var iHuM = 0; iHuM < aHuMat.length; iHuM++) {
				oData.aList.push({
					Bismt: aHuMat[iHuM].Bismt,
					Ean11: aHuMat[iHuM].Ean11,
					Exidv: aHuMat[iHuM].Exidv,
					Maktx: aHuMat[iHuM].Maktx,
					Matnr: aHuMat[iHuM].Matnr,
					NegoCedi: aHuMat[iHuM].NegoCedi,
					QRead: parseInt(Number(aHuMat[iHuM].QRead), 10).toString(),
					QSca: parseInt(Number(aHuMat[iHuM].QSca), 10).toString(),
					Stock: parseInt(Number(aHuMat[iHuM].Stock), 10).toString()
				});
			}

			oModel.setData(oData);
			oModel.refresh(true);
		}

	});

});