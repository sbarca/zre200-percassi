/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View1", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this.getRouter().getRoute("View1").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		_onObjectMatched: function () {
			//When the view is reached, all models are cleaned
			this._cleanHuModel();
			this._cleanHuMatModel();
			this._cleanMatModel();
		},

		_onPress: function () {
			//NegoCedi = '3' for this application
			var oModel = this.getModel("HuModel");
			var oData = oModel.getData();
			oData.NegoCedi = "3";
			oModel.setData(oData);
			oModel.refresh(true);
			this.getRouter().navTo("View2");
		}

	});

});