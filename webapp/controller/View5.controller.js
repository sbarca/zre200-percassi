/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View5", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this._filterModel = new JSONModel({
				Filter: ""
			});
			this.getView().setModel(this._filterModel, "filter");
			this.getRouter().getRoute("View5").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},
		
		_onObjectMatched: function () {
			//When the view is reached, Mat model is cleaned
			
			this._cleanMatModel();
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		_onPressListaDiff: function () {
			this._onRefresh();
			this.getRouter().navTo("View6");
		},

		_onPressContaFinita: function () {
			this._onRefresh();
			this.getRouter().navTo("View7");
		},

		_onFilter: function () {
			var sFilter = this.getView().getModel("filter").getData().Filter;
			var oTable = this.getView().byId("mTab5");

			var oBinding = oTable.getBinding("items"),
				aFilters = [];

			var oFilter = new sap.ui.model.Filter("Ean11",
				sap.ui.model.FilterOperator.EQ,
				sFilter
			);
			// var oFilter = new Filter("Ean11", sap.ui.model.FilterOperator.EQ, sFilter);
			aFilters.push(oFilter);

			// apply filter settings
			oBinding.filter(aFilters);

		},

		_onRefresh: function () {
			var oData = this.getView().getModel("filter").getData();
			oData.Filter = "";
			this.getView().getModel("filter").setData(oData);
			this.getView().getModel("filter").refresh(true);

			var oTable = this.getView().byId("mTab5");

			var oBinding = oTable.getBinding("items"),
				aFilters = [];

			var oFilter = new sap.ui.model.Filter("Ean11",
				sap.ui.model.FilterOperator.Contains,
				""
			);

			// var oFilter = new Filter("Ean11", sap.ui.model.FilterOperator.Contains, "");
			aFilters.push(oFilter);

			// apply filter settings
			oBinding.filter(aFilters);
		},

		_onChange: function (oEvent) {
			var sPath = oEvent.getSource().getParent().getBindingContextPath();
			// var sValue = oEvent.getSource().getValue();
			var nIndSub = sPath.lastIndexOf("/");
			var nInd = sPath.substring(nIndSub + 1);
			var oHuMatModel = this.getView().getModel("HuMatModel");
			var oData = oHuMatModel.getData();
			var oMat = oData.aList[nInd];
			var oModel = this.getView().getModel();
			var sPathUp = oModel.createKey("/HuMatSet", {
				Exidv: oMat.Exidv,
				NegoCedi: oMat.NegoCedi,
				Ean11: oMat.Ean11
			});

			oModel.update(sPathUp, oMat, {
				success: function (oD) {
					/*oMatData.Exidv = oMat.Exidv;
					oMatData.Matnr = oMat.Matnr;
					oMatData.Bismt = oMat.Bismt;
					oMatData.Maktx = oMat.Maktx;
					oMatData.Stock = oMat.Stock;
					oMatData.QSca = oMat.QSca;
					oMatData.QRead = oMat.QRead;
					oHuMatData.aList[nInd].QRead = oMat.QRead;
					oHuMatModel.setData(oHuMatData);
					oHuMatModel.refresh(true);*/
				},

				error: function (oErr) {

				}
			});

		}

	});

});