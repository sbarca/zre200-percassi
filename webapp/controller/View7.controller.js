/*global history */
sap.ui.define([
	"ZRE200_UI_FHU/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"ZRE200_UI_FHU/model/formatter",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, MessageBox) {
	"use strict";

	return BaseController.extend("ZRE200_UI_FHU.controller.View7", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function () {
			this._esitoModel = new JSONModel({
				Esito: ""
			});
			this.getView().setModel(this._esitoModel, "esito");
			this.getRouter().getRoute("View7").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			//this._onObjectMatched(oEvent);

		},

		_onObjectMatched: function () {
			var oHuMatModel = this.getView().getModel("HuMatModel");
			var aList = oHuMatModel.getData().aList;
			var oModel = this.getView().getModel("esito");
			var oData = oModel.getData();
			var i18n = this.getResourceBundle();
			var that = this;

			var fnFilter = function (oValue) {
				var fnFindMat = function (oM) {
					return oM.Ean11 === oValue.Ean11;
				};
				var oHuMatM = this.getView().getModel("HuMatModel");
				var oHuMatData = oHuMatM.getData();
				var nInd = oHuMatData.aList.findIndex(fnFindMat);
				return oHuMatData.aList[nInd].QSca !== oHuMatData.aList[nInd].QRead;
			};

			var aFiltList = aList.filter(fnFilter, that);
			if(aFiltList.length > 0) {
				oData.Esito = i18n.getText("EsitoNO");
			} else {
				oData.Esito = i18n.getText("EsitoYES");
			}
			oModel.setData(oData);
			oModel.refresh(true);
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		_onPressConfermaContaFinita: function () {
			var oModel = this.getView().getModel();
			var oDataModel = this.getView().getModel("HuModel").getData();
			var oUrlParams = {
				Exidv : oDataModel.Exidv,
				NegoCedi : oDataModel.NegoCedi
			};
			oModel.callFunction("/CloseHu", {
				method: "POST",
				urlParameters: oUrlParams,
				success: jQuery.proxy(this.successGetATPData, this),
				error: jQuery.proxy(this.errorGetATPData, this)

			});
			// this.getRouter().navTo("View8");
		},
		
		successGetATPData: function (oData) {
			this.getRouter().navTo("View8");
		},
		
		_onPressSalva: function () {
			this.getRouter().navTo("View1");
		}

	});

});